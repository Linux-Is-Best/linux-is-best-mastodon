# Linux-Is-Best-Mastodon

This is both my follow, block, and mute list for Mastodon.  

**NOTE** The Facebook (Meta) domain list is better on a server level and **not** on an individual account.

**Sharkey**: My primary account uses Sharkey, a fork of Misskey.
The format is different and you can find that list here: https://gitlab.com/Linux-Is-Best/sharkey_list

**2nd Account**: I joined the Fediverse before Vivaldi did, which uses Mastodon. 
But I also had a Vivaldi account before Vivaldi joined the Fediverse. 
Ultimately, that is how this account came into being: https://social.vivaldi.net/@NetscapeNavigator
